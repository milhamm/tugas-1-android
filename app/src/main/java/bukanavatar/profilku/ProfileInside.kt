package bukanavatar.profilku

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class ProfileInside : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_inside)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        title = "More Profile"
    }
}
